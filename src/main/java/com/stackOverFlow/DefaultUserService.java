package com.stackOverFlow;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.FormLoginHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.impl.RabbitMQClientImpl;

public abstract class DefaultUserService extends RestAPIVerticle {
	
	public static final String MONGO_PORT = ":27017";
	
	public static final String MONGODB_HOST = "MONGODB_HOST";
	
	protected static final int DEFAULT_PORT = 8050;

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();
		Router router = Router.router(vertx);
		// body handler
		router.route().handler(CookieHandler.create());
		router.route().handler(BodyHandler.create());
		createSessionHandler(router);
		vertx.deployVerticle("com.stackOverFlow.UserVerticle", new DeploymentOptions().setWorker(true).setInstances(2));

		JsonObject config = new JsonObject();
		config.put("db_name", "stackoverflow");
		// config.put("connection_string", "mongodb://" + MONGODB + ":27017");
		config.put("connection_string", "mongodb://" + getMongoDBHost() + MONGO_PORT);
		MongoClient client = MongoClient.createShared(vertx, config);
		JsonObject authProperties = new JsonObject();
		authProperties.put(MongoAuth.PROPERTY_COLLECTION_NAME, "users");
		authProperties.put(MongoAuth.PROPERTY_USERNAME_FIELD, "_id");
		// MongoAuth authProvider = MongoAuth.create(client, authProperties);
		MongoAuth authProvider = new MyAuthProvider(client, authProperties);

		// Create a JWT Auth Provider
		JWTAuth jwt = JWTAuth.create(vertx, new JsonObject().put("keyStore",
				new JsonObject().put("type", "jceks").put("path", "keystore.jceks").put("password", "secret")));

		Handler<RoutingContext> handler = JWTAuthHandler.create(jwt);
		router.route(HttpMethod.GET, "/users/").handler(handler);
		router.route(HttpMethod.GET, "/users/logout/").handler(handler);
		router.route(HttpMethod.GET, "/users/profile/").handler(handler);
		router.route(HttpMethod.PUT, "/users/").handler(handler);
//		System.out.println("DefaultUserService.start() authProvider: " + authProvider);
		router.route("/users/login")
				.handler(new CustomFormLoginHandler(authProvider, FormLoginHandler.DEFAULT_USERNAME_PARAM,
						FormLoginHandler.DEFAULT_PASSWORD_PARAM, FormLoginHandler.DEFAULT_RETURN_URL_PARAM, null, jwt));

		router.get("/users/").handler(rctx -> {
			System.out.println("DefaultUserService.start() GET USERS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			System.out.println("DefaultUserService.start() rctx.user().principal(): " + rctx.user().principal());
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/users/profile/").handler(rctx -> {
			System.out.println("DefaultUserService.start() GET USERS DETAIL rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			System.out.println("DefaultUserService.start()USERS DETAIL rctx.user().principal(): " + rctx.user().principal());
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				System.out.println("DefaultUserService.start() USERS DETAIL res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/users/profile/:userId").handler(rctx -> {
			System.out.println("APIGateway_2.start() USER profile:::::::::::: "+rctx.request().getParam("userId"));
			JsonObject jsonObj = new JsonObject().put("username", rctx.request().getParam("userId"))
					.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", jsonObj, res -> {
				System.out.println("DefaultUserService.start() USERS PROFILE res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.post("/users/").handler(rctx -> {
			System.out.println("DefaultUserService.start() POST USERS rctx.getBodyAsJson().encodePrettily(): "
					+ rctx.getBodyAsJson().encodePrettily());
			JsonObject userJsonObj = rctx.getBodyAsJson();
			userJsonObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("write.users", userJsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.put("/users/").handler(rctx -> {
			System.out.println("DefaultUserService.start() PUT USERS rctx.getBodyAsJson().encodePrettily(): "
					+ rctx.getBodyAsJson().encodePrettily());
			JsonObject userJsonObj = rctx.getBodyAsJson();
			userJsonObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("update.users", userJsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					userJsonObj.remove("MONGODB_HOST");
//					RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://35.190.147.51:5672"));
					RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://10.197.54.23:8090"));
//					RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://104.196.128.2:8090"));
					rabbit.start(response -> {
						JsonObject message = new JsonObject();
						message.put("properties", new JsonObject().put("contentType", "application/json"));
						message.put("body", userJsonObj);
						rabbit.basicPublish("", "update.user", message, result -> {
							if (result.succeeded()) {
								System.out.println("Message Published!");
							} else {
								System.out.println("Failed");
								result.cause().printStackTrace();
							}
						});
					});
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.get("/users/logout/").handler(rctx -> {
			System.out
					.println("DefaultUserService.start() Authorization header: " + rctx.request().getHeader("Authorization"));
			rctx.clearUser();
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html").end();
		});

		router.route(HttpMethod.POST, "/questions/").handler(handler);
//		router.route(HttpMethod.PATCH, "/questions/").handler(handler);
		router.route(HttpMethod.PUT, "/questions/").handler(handler);

		router.route(HttpMethod.POST, "/questions/").handler(rctx -> {
			System.out.println("DefaultUserService.start() POST USERS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				if (res.succeeded()) {
					JsonObject resJson = new JsonObject(res.result().body().toString());
					JsonObject usernameObj = new JsonObject();
					usernameObj.put("username", resJson.getString("_id"));
					usernameObj.put("name", resJson.getString("name"));
					usernameObj.put("city", resJson.getString("city"));
					rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
							.end(usernameObj.encodePrettily());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

//		router.route(HttpMethod.PATCH, "/questions/").handler(rctx -> {
		router.route(HttpMethod.PUT, "/questions/").handler(rctx -> {
			System.out.println("DefaultUserService.start() PATCH USERS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				if (res.succeeded()) {
					JsonObject resJson = new JsonObject(res.result().body().toString());
					JsonObject usernameObj = new JsonObject();
					usernameObj.put("username", resJson.getString("_id"));
					usernameObj.put("name", resJson.getString("name"));
					usernameObj.put("city", resJson.getString("city"));
					rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
							.end(usernameObj.encodePrettily());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		createHttpServer(router, future);
	}

	protected abstract void createSessionHandler(final Router router);
	
	protected abstract String getMongoDBHost();
	
	protected abstract void createHttpServer(final Router router,Future<Void> future);

}
